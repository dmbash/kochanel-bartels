import { Component, ViewChild, ElementRef } from '@angular/core';
import { PointStorage } from './classes/point-storage';
import { Point } from './classes/point';
import { AbstractPoint } from './classes/abstractPoint';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  private TANGENT_POINT_COLOR = 'blue';
  private POINT_COLOR = 'black';
  private SELECTED_POINT_COLOR = 'green';

  private _modalXVal: number;
  public get modalXVal(): number {
    return this._modalXVal;
  }
  public set modalXVal(v: number) {
    this._modalXVal = v;
  }

  private _modalYVal: number;
  public get modalYVal(): number {
    return this._modalYVal;
  }
  public set modalYVal(v: number) {
    this._modalYVal = v;
  }

  private _modalVisible: boolean;
  public get modalVisible(): boolean {
    return this._modalVisible;
  }
  public set modalVisible(v: boolean) {
    this._modalVisible = v;
  }

  private _selectedPoint: Point;
  public get selectedPoint(): Point {
    return this._selectedPoint;
  }
  public set selectedPoint(v: Point) {
    this._selectedPoint = v;
  }

  @ViewChild('drawingField')
  drawingField: ElementRef<HTMLCanvasElement>;

  context: CanvasRenderingContext2D;
  rows: Object[] = new Array<Object>();
  pS = new PointStorage();
  selectedRow = -1;

  // spline default params
  c = 1;
  t = 1;
  b = 1;

  ngAfterViewInit() {
    this.modalVisible = false;
    this.context = this.drawingField.nativeElement.getContext('2d');
  }

  onParamsChange() {
    this.canvasHandler().updateCanvas();
  }

  canvasHandler() {
    const THIS = this;
    return {
      drawPoints: () => {
        const points = THIS.pS.storage;
        points.forEach((point) => {
          const { X, Y, W, H, tangent } = point;
          if (tangent) {
            THIS.context.fillStyle = this.TANGENT_POINT_COLOR;
          } else {
            THIS.context.fillStyle = this.POINT_COLOR;
          }
          THIS.context.fillRect(X, Y, W, H);
        });
      },
      drawSpline: () => {

        function h1(s: number) {
          return (2 * Math.pow(s, 3) - 3 * Math.pow(s, 2) + 1);
        }

        function h2(s: number) {
          return (-2 * Math.pow(s, 3) + 3 * Math.pow(s, 2));
        }

        function h3(s: number) {
          return (Math.pow(s, 3) - 2 * Math.pow(s, 2) + s);
        }

        function h4(s: number) {
          return (Math.pow(s, 3) - Math.pow(s, 2));
        }


        if (typeof (THIS.t) !== 'undefined' && typeof(THIS.b) !== 'undefined' && typeof(THIS.c) !== 'undefined') {
          const originalLength = THIS.pS.length();
          if (originalLength > 3) {
            const delta = 0.01;

            THIS.context.fillStyle = 'yellow';
            const dStartConstants = {
              firstParamPart: ((1 - THIS.t) * (1 + THIS.b) * (1 + THIS.c)) / 2,
              secondParamPart: ((1 - THIS.t) * (1 - THIS.b) * (1 - THIS.c)) / 2,
            };

            const dEndConstants = {
              firstParamPart: ((1 - THIS.t) * (1 + THIS.b) * (1 - THIS.c)) / 2,
              secondParamPart: ((1 - THIS.t) * (1 - THIS.b) * (1 + THIS.c)) / 2,
            };

            const startTangent = THIS.pS.storage[1];
            const endTangent = THIS.pS.storage[originalLength - 1];
            const points = THIS.pS.storage.filter((point) => (!point.tangent));
            const length = points.length;

            for (let i = 0; i <= length - 2; i++) {

              const pI = points[i];
              const pIMinus1 = points[i - 1];
              const pIPlus1 = points[i + 1];
              const pIPlus2 = points[i + 2];
              let dStart: any;
              let dEnd: any;

              if (i === 0) {
                dStart = {
                  x: startTangent.X - pI.X,
                  y: startTangent.Y - pI.Y,
                };
              } else {
                // // dStart = ( ( (1-t) * (1+b) * (1+c) ) / 2) * (P[i] - P[i-1]) + ( ( (1-t) * (1-b) * (1-c) ) / 2) * (P[i+1]-P[i])
                const firstPart = [pI.X - pIMinus1.X, pI.Y - pIMinus1.Y];
                const secondPart = [pIPlus1.X - pI.X, pIPlus1.Y - pI.Y];
                const firstComponent = [
                  dStartConstants.firstParamPart * firstPart[0],
                  dStartConstants.firstParamPart * firstPart[1],
                ];
                const secondComponent = [
                  dStartConstants.secondParamPart * secondPart[0],
                  dStartConstants.secondParamPart * secondPart[1],
                ];
                dStart = {
                  x: firstComponent[0] + secondComponent[0],
                  y: firstComponent[1] + secondComponent[1],
                };
              }

              if (i === length - 2) {
                dEnd = {
                  x: endTangent.X - pIPlus1.X,
                  y: endTangent.Y - pIPlus1.Y,
                };
              } else {
                // dEnd = ( ( (1-t) * (1+b) * (1-c) ) / 2) * (P[i+1] - P[i]) + ( ( (1-t) * (1-b) * (1+c) ) / 2) * (P[i+2]-P[i+1])
                const firstPart = [pIPlus1.X - pI.X, pIPlus1.Y - pI.Y];
                const secondPart = [pIPlus2.X - pIPlus1.X, pIPlus2.Y - pIPlus2.Y];
                const firstComponent = [
                  dEndConstants.firstParamPart * firstPart[0],
                  dEndConstants.firstParamPart * firstPart[1],
                ];
                const secondComponent = [
                  dEndConstants.secondParamPart * secondPart[0],
                  dEndConstants.secondParamPart * secondPart[1],
                ];
                dEnd = {
                  x: firstComponent[0] + secondComponent[0],
                  y: firstComponent[1] + secondComponent[1],
                };
              }

              for (let s = 0; s <= 1; s += delta) {
                const first = [pI.X * h1(s), pI.Y * h1(s)];
                const second = [pIPlus1.X * h2(s), pIPlus1.Y * h2(s)];
                const third = [dStart.x * h3(s), dStart.y * h3(s)];
                const fourth = [dEnd.x * h4(s), dEnd.y * h4(s)];
                const Ps = {
                  x: first[0] + second[0] + third[0] + fourth[0],
                  y: first[1] + second[1] + third[1] + fourth[1],
                };
                THIS.context.fillRect(Ps.x, Ps.y, 5, 5);
                // Ps = P[i] * h_1(s)+P[i+1]*h_2(s)+dStart*h_3(s)+dEnd*h_4(s)
              }
            }
          }
        } else {
          throw Error('Spline params not given');
        }
      },
      clearCanvas: () => {
        THIS.context.clearRect(
          0,
          0,
          THIS.drawingField.nativeElement.clientWidth,
          THIS.drawingField.nativeElement.clientHeight
        );
      },
      updateCanvas: () => {
        THIS.context.fillStyle = this.POINT_COLOR;
        THIS.canvasHandler().clearCanvas();
        THIS.canvasHandler().drawPoints();
        if (THIS.pS.length() > 3) {
          THIS.canvasHandler().drawSpline();
        }
      },
      canvasMouseUp: (event) => {
        const [clickX, clickY] = [event.layerX, event.layerY];
        if (THIS.selectedPoint) {
          THIS.selectedPoint.X = clickX - THIS.selectedPoint.W / 2;
          THIS.selectedPoint.Y = clickY - THIS.selectedPoint.H / 2;
          THIS.pointHandler().updatePoint();
          THIS.pointHandler().selectPoint(THIS.selectedPoint);
        } else {
          const x = clickX - AbstractPoint.DEFAULT_W / 2;
          const y = clickY - AbstractPoint.DEFAULT_H / 2;
          if (this.pS.length() === 1 || this.pS.length() >= 3) {
            this.pS.storage[this.pS.length() - 1].tangent = false;
            this.pointHandler().addPoint(x, y, true);
          } else {
            this.pointHandler().addPoint(x, y);
          }
        }
      },
      canvasMouseDown: (event) => {
        const [clickX, clickY] = [event.layerX, event.layerY];
        const points = THIS.pS.storage;
        points.forEach((point) => {
          if (point.intersectWith(new AbstractPoint(clickX, clickY))) {
            THIS.pointHandler().selectPoint(point);
          }
        });
      },
    };
  }

  buttonsHandler() {
    const THIS = this;

    function toggleModal(): void {
      THIS.modalVisible = !THIS.modalVisible;
    }

    return {
      addButton: () => {
        return toggleModal();
      },
      closeModalButton: () => {
        return toggleModal();
      },
      applyModalButton: () => {
        THIS.pointHandler().addPoint(THIS.modalXVal, THIS.modalYVal);
        this.modalXVal = null;
        this.modalYVal = null;
        toggleModal();
      },
      deleteButton: () => {
        if (THIS.selectedPoint) {
          THIS.pointHandler().deletePoint(THIS.selectedPoint);
          THIS.selectedPoint = null;
        }
      },
    };
  }

  pointHandler() {
    const THIS = this;

    function highlightHandler(p: Point, action: string) {
      const { X, Y, W, H, tangent } = p;
      THIS.context.fillStyle =
        action === 'apply'
          ? THIS.SELECTED_POINT_COLOR
          : tangent
          ? THIS.TANGENT_POINT_COLOR
          : THIS.POINT_COLOR;
      THIS.context.fillRect(X, Y, W, H);
    }

    return {
      addPoint: (x: number, y: number, tangent?: boolean) => {
        let p: Point;
        if (tangent) {
          p = Point.isTangent(new Point(x, y));
        } else {
          p = new Point(x, y);
        }
        THIS.pS.addPoint(p);
        THIS.canvasHandler().updateCanvas();
      },
      deletePoint: (p: Point) => {
        const wasStartTangent = this.pS.storage[1] === p;
        this.pS.deletePoint(p);
        if (wasStartTangent && this.pS.length() > 1) {
          this.pS.storage[1].tangent = true;
        } else if (this.pS.length() > 3) {
          this.pS.storage[this.pS.length() - 1].tangent = true;
        }
        THIS.canvasHandler().updateCanvas();
      },
      selectPoint: (p: Point) => {
        if (THIS.selectedPoint === p) {
          THIS.selectedPoint = null;
          THIS.selectedRow = -1;
          highlightHandler(p, 'clear');
        } else {
          if (THIS.selectedPoint) {
            THIS.selectedRow = -1;
            highlightHandler(THIS.selectedPoint, 'clear');
          }
          THIS.selectedPoint = p;
          THIS.selectedRow = p.ID;
          highlightHandler(p, 'apply');
        }
      },
      updatePoint: () => {
        THIS.canvasHandler().updateCanvas();
      },
    };
  }
}
