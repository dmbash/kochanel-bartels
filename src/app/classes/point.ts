import { AbstractPoint } from './abstractPoint';

export class Point extends AbstractPoint {

  static currentIndex = 0;

  private _ID: number;
  public get ID(): number {
    return this._ID;
  }


  private _tangent : boolean;
  public get tangent() : boolean {
    return this._tangent;
  }
  public set tangent(v : boolean) {
    this._tangent = v;
  }


  constructor(x: number, y: number, w?: number, h?: number, id?: number) {
    super(x, y, w, h);
    if (id) {
      this._ID = id;
    } else {
      this._ID = Point.incIndex();
    }
  }

  public static isTangent(p: Point): Point {
    p.tangent = true;
    return p;
  }

  static incIndex(): number {
    const index = Point.currentIndex;
    Point.currentIndex += 1;
    return index;
  }

  intersectWith(p: AbstractPoint): boolean {
    let intersection = false;
    if (p.X >= this.X && p.X <= this.X + this.W) {
      if (p.Y >= this.Y && p.Y <= this.Y + this.H) {
        intersection = true;
      }
    }
    return intersection;
  }
}
