export class AbstractPoint {

  static DEFAULT_W = 10;
  static DEFAULT_H = 10;

  private _X: number;
  public get X(): number {
    return this._X;
  }
  public set X(v: number) {
    this._X = v;
  }

  private _Y: number;
  public get Y(): number {
    return this._Y;
  }
  public set Y(v: number) {
    this._Y = v;
  }

  private _W: number;
  public get W(): number {
    return this._W;
  }
  public set W(v: number) {
    this._W = v;
  }

  private _H: number;
  public get H(): number {
    return this._H;
  }
  public set H(v: number) {
    this._H = v;
  }

  constructor(x: number, y: number, w: number= AbstractPoint.DEFAULT_W, h: number= AbstractPoint.DEFAULT_H) {
    this.X = x;
    this.Y = y;
    this.W = w;
    this.H = h;
  }
}
